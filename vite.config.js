import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

const mode = process.env.NODE_ENV;
const isProd = mode === 'production';
const config = {
  mode,
  resolve: {
    alias: {
      '@components': path.resolve(__dirname, 'src/components'),
      '@utils': path.resolve(__dirname, 'src/utils'),
      '@api': path.resolve(__dirname, 'src/api'),
      '@types': path.resolve(__dirname, 'src/types'),
      '@store': path.resolve(__dirname, 'src/store'),
      '@lib': path.resolve(__dirname, 'lib')
    }
  },
  define: {
    'process.env': { API_URL: '' }
  },
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd,
    rollupOptions: {
      input: {
        react: path.resolve(__dirname, 'src')
      },
      output: {
        assetFileNames: assetInfo => {
          let [, ext] = assetInfo.name.split('.');

          if ((/png|jpe?g|svg|gif/i).test(ext)) {
            ext = 'img';
          }
          return `${ext}/[name][hash][extname]`;
        }
      }
    },
    plugins: [
      react({
        babel: {
          plugins: [
            ['@babel/plugin-proposal-decorators', { 'legacy': true }],
            ['@babel/plugin-proposal-class-properties']
          ]
        }
      })
    ]
  }
};

export default defineConfig(config);
