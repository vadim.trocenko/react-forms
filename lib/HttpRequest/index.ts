type TConfig = {
  method?: string,
  url?: string,
  headers?: { [key: string]: string },
  data?: FormData,
  responseType?: string
}

type TResponseTypes = {
  [key: string]: (response: Response) => unknown
}

const responseTypeHandlers: TResponseTypes = {
  'json': response => response.json(),
  'text': response => response.text(),
  'arrayBuffer': response => response.arrayBuffer(),
  'blob': response => response.blob()
};

class HttpRequest {
  constructor(public baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  get<T>(url: string, config?: TConfig) {
    return this.request<T>(Object.assign({}, config, { method: 'GET', url }));
  }

  post<T>(url: string, data?: FormData, config?: TConfig) {
    return this.request<T>(Object.assign({}, config, { method: 'POST', url, data }));
  }

  delete<T>(url: string, config?: TConfig) {
    return this.request<T>(Object.assign({}, config, { method: 'DELETE', url }));
  }

  request<T>(config: TConfig) {
    const {
      method = 'GET',
      url,
      headers,
      data,
      responseType = 'text'
    } = config;

    if (!url) {
      throw new Error('Url is not defined');
    }

    const baseUrl = new URL(url, this.baseUrl);

    return fetch(baseUrl.href, {
      method,
      headers,
      body: data
    })
      .then(res => {
        if (!res.ok) {
          throw new Error(`${method} ${res.url} ${res.statusText}`);
        }

        return responseTypeHandlers[responseType](res);
      }) as Promise<T>;
  }
}

export default HttpRequest;
