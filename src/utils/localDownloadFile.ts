export default function localDownloadFile(path: string, file: string) {
  const a = document.createElement('a');

  a.download = file;
  a.href = path;
  a.click();
}
