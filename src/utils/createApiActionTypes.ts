export default function createApiActionTypes(actionName: string) {
  return {
    PENDING: `${actionName}Pending`,
    SUCCESS: `${actionName}Success`,
    FAILURE: `${actionName}Failure`
  };
}
