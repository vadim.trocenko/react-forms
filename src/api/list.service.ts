import HttpRequest from '@lib/HttpRequest';

const BASE_URL = process.env.API_URL || 'http://localhost:8000';
const http = new HttpRequest(BASE_URL);

function loadFileList<T>() {
  return http.get<T>('/list', { responseType: 'json' });
}

function uploadFile(data: FormData) {
  return http.post<string>('/upload', data);
}

function downloadFile(path: string) {
  return http.get<Blob>(`/files/${path}`, { responseType: 'blob' });
}

function removeFile(path: string) {
  return http.delete<string>(`/list/${path}`).then(() => path);
}

export {
  loadFileList,
  uploadFile,
  downloadFile,
  removeFile
};
