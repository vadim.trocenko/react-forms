import { TDispatch } from 'react-like';

export type TFileItem = {
  name: string,
  extension: string,
  fileSizeInBytes: number
}

export type TThunkFunction = (...args: Array<unknown>) => (dispatch: TDispatch<unknown>) => Promise<void>

export type TEventTargetSampleFile ={
  sampleFile: HTMLInputElement
} & EventTarget
