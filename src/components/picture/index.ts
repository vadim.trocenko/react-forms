import { BaseComponent } from 'react-like';
import { connect, TStore } from '@store';
import './style.css';

export type TPictureProps = {
  imgUrl: string
}

const mapStateToProps = ({ list: { imgUrl } }: TStore) => ({
  imgUrl
});

@connect(mapStateToProps)
export default class PictureComponent extends BaseComponent<TPictureProps> {
  static propTypes = {
    imgUrl: 'string'
  };

  render() {
    this.node.innerHTML = this.props.imgUrl ? `
      <div class="picture_wrapper">
        <img class="picture" src="${this.props.imgUrl}" alt="Picture" />
      </div>
    ` : '';
  }
}
