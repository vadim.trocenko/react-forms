import { API_REQUEST } from 'react-like';
import { TFileItem } from '@types';
import { listService } from '@api';
import { createApiActionTypes } from '@utils';

export const LIST_GET_ACTION_TYPES = createApiActionTypes('getList');
export const REMOVE_FILE_ACTION_TYPES = createApiActionTypes('deleteFile');
export const SET_SELECTED_ACTION_TYPE = 'setSelectedItem';

const loadListOfFilesAction = () => ({
  type: API_REQUEST,
  types: [
    LIST_GET_ACTION_TYPES.PENDING,
    LIST_GET_ACTION_TYPES.SUCCESS,
    LIST_GET_ACTION_TYPES.FAILURE
  ],
  request: () => listService.loadFileList<TFileItem[]>()
});

const deleteFileAction = (fileName: string) => ({
  type: API_REQUEST,
  types: [
    REMOVE_FILE_ACTION_TYPES.PENDING,
    REMOVE_FILE_ACTION_TYPES.SUCCESS,
    REMOVE_FILE_ACTION_TYPES.FAILURE
  ],
  request: () => listService.removeFile(fileName)
});

const setSelectedItemAction = (fileName: string) => ({
  type: SET_SELECTED_ACTION_TYPE,
  data: fileName
});

export { loadListOfFilesAction, deleteFileAction, setSelectedItemAction };
