/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { BaseComponent } from 'react-like';
import { TFileItem } from '@types';
import { connect, TStore } from '@store';
import {
  loadListOfFilesAction,
  deleteFileAction,
  setSelectedItemAction
} from './actions';
import './style.css';

export type TListProps = {
  data: Array<TFileItem>,
  selectedItem: string,
  loadListOfFilesAction: () => void,
  deleteFileAction: (path: string) => void,
  setSelectedItemAction: (item: string) => void
}

const mapStateToProps = ({ list: { data, selectedItem } }: TStore) => ({
  data,
  selectedItem
});

const mapDispatchToProps = {
  loadListOfFilesAction,
  deleteFileAction,
  setSelectedItemAction
};

@connect<TListProps>(mapStateToProps, mapDispatchToProps)
export default class ListComponent extends BaseComponent<TListProps> {
  static propTypes = {
    data: 'Array<object>',
    selectedItem: 'string',
    loadListOfFilesAction: 'function',
    deleteFileAction: 'function',
    setSelectedItemAction: 'function'
  };

  deleteFile = ({ target }: Event) => {
    const { fileName } = ((target as HTMLElement).parentNode as HTMLElement).dataset;

    this.props.deleteFileAction(fileName!);
  };

  selectFile = ({ target }: Event) => {
    const { fileName } = ((target as HTMLElement).parentNode as HTMLElement).dataset;

    this.props.setSelectedItemAction(fileName!);
  };

  componentDidMount() {
    this.props.loadListOfFilesAction();
  }

  render() {
    this.node.innerHTML = `
      <ul class="list">
        ${this.props.data.reduce((acc: string, item: TFileItem) => `${acc}
          <li data-file-name=${item.name} class="list_element">
            <span class="element_name">${item.name}</span>
            <span
              class="element_delete"
              type="button"
              value="Delete!">×
            </span>
          </li>
        `, '') || '<li class="list__item">No files</li>'}
      </ul>
    `;

    const deleteButtons = this.node.querySelectorAll('.element_delete');
    const fileNames = this.node.querySelectorAll('.element_name');

    for (let i = 0; i < deleteButtons.length; i++) {
      deleteButtons[i].addEventListener('click', this.deleteFile);
      fileNames[i].addEventListener('click', this.selectFile);
    }
  }
}
