import { API_REQUEST } from 'react-like';
import { listService } from '@api';
import { localDownloadFile, createApiActionTypes } from '@utils';

export const DOWNLOAD_FILE_ACTION_TYPES = createApiActionTypes('downloadFile');

const downloadFileAction = (fileName: string) => ({
  type: API_REQUEST,
  types: [
    DOWNLOAD_FILE_ACTION_TYPES.PENDING,
    DOWNLOAD_FILE_ACTION_TYPES.SUCCESS,
    DOWNLOAD_FILE_ACTION_TYPES.FAILURE
  ],
  request: () => listService.downloadFile(fileName).then(res => {
    const url = URL.createObjectURL(res);

    if (res.type.includes('image')) {
      return url;
    }

    localDownloadFile(url, fileName);
  })
});

export { downloadFileAction };
