import { BaseComponent } from 'react-like';
import { TEventTargetSampleFile } from '@types';
import { connect, TStore } from '@store';
import { downloadFileAction } from './action';
import './style.css';

export type TDownloadProps = {
  downloadFileAction: (file: string) => void,
  selectedItem: string
}

const mapStateToProps = ({ list: { selectedItem } }: TStore) => ({
  selectedItem
});

const mapDispatchToProps = {
  downloadFileAction
};

@connect(mapStateToProps, mapDispatchToProps)
export default class DownloadFormComponent extends BaseComponent<TDownloadProps> {
  static propTypes = {
    downloadFileAction: 'function',
    selectedItem: 'string'
  };

  submit = (e: Event) => {
    e.preventDefault();

    const data = (e.target as TEventTargetSampleFile).sampleFile.value;

    if (!data) {
      return;
    }

    this.props.downloadFileAction(data);
  };

  render() {
    this.node.innerHTML = `
      <form class="download">
        <input class="download_input" placeholder="Enter name of file..." type="text" name="sampleFile" />
        <button class="download_button" type="submit" value="Download!">Download</button>
      </form>
    `;

    const downloadForm = this.node.querySelector('.download') as HTMLFormElement;
    const input = this.node.querySelector('.download_input') as HTMLInputElement;

    input.value = this.props.selectedItem;
    downloadForm.addEventListener('submit', this.submit);
  }
}
