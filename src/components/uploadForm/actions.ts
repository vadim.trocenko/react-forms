import { TDispatch } from 'react-like';
import { listService } from '@api';
import { createApiActionTypes } from '@utils';
import { loadListOfFilesAction } from '../list/actions';

export const UPLOAD_FILE_ACTION_TYPES = createApiActionTypes('uploadFile');

const uploadFileAction = (file: FormData) => (dispatch: TDispatch<unknown>) =>
  listService.uploadFile(file).then(() => {
    dispatch(loadListOfFilesAction());
  });

export { uploadFileAction };
