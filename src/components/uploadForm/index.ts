import { BaseComponent } from 'react-like';
import { TEventTargetSampleFile, TThunkFunction } from '@types';
import { connect } from '@store';
import { uploadFileAction } from './actions';
import './style.css';

export type TUploadProps = {
  uploadFileAction: TThunkFunction;
}

const mapDispatchToProps = {
  uploadFileAction
};

@connect<TUploadProps>(null, mapDispatchToProps)
export default class UploadFormComponent extends BaseComponent<TUploadProps> {
  static propTypes = {
    uploadFileAction: 'function'
  };

  submit = (e: Event) => {
    e.preventDefault();

    const formData = new FormData();

    if (!e.target) {
      return;
    }

    const { files } = (e.target as TEventTargetSampleFile).sampleFile;

    if (!files) {
      return;
    }

    formData.append('sampleFile', files[0]);
    this.props.uploadFileAction(formData);
  };

  render() {
    this.node.innerHTML = `
    <form class="upload">
      <input class="upload_input" type="file" name="sampleFile" />
      <button
        class="upload_button"
        type="submit"
        value="Upload!">Upload
      </button>
    </form>
    `;

    const uploadForm = this.node.querySelector('.upload') as HTMLFormElement;

    uploadForm.addEventListener('submit', this.submit);
  }
}
