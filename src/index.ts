/* eslint-disable @typescript-eslint/no-unused-vars */
import './style.css';
import {
  PictureComponent,
  DownloadFormComponent,
  ListComponent,
  UploadFormComponent
} from '../src/components';
import { TListProps } from './components/list/';
import { TUploadProps } from './components/uploadForm/';
import { TDownloadProps } from './components/downloadForm/';
import { TPictureProps } from './components/picture/';

const root = document.querySelector('#root') as HTMLElement;
const wrapperElements: HTMLElement = document.createElement('div');
const mainContent: HTMLElement = document.createElement('div');
const listContainer: HTMLElement = document.createElement('div');
const uploadFormContainer: HTMLElement = document.createElement('div');
const downloadFormContainer: HTMLElement = document.createElement('div');
const pictureContainer: HTMLElement = document.createElement('div');

listContainer.classList.add('list-container');
uploadFormContainer.classList.add('upload-container');
downloadFormContainer.classList.add('download-container');
pictureContainer.classList.add('picture-container');
wrapperElements.classList.add('wrapper');
mainContent.classList.add('main-content');

mainContent.append(uploadFormContainer, downloadFormContainer, pictureContainer, listContainer);
wrapperElements.append(mainContent);
root.append(wrapperElements);

const list = new ListComponent({} as TListProps, listContainer);
const uploadForm = new UploadFormComponent({} as TUploadProps, uploadFormContainer);
const downloadForm = new DownloadFormComponent({} as TDownloadProps, downloadFormContainer);
const picture = new PictureComponent({} as TPictureProps, pictureContainer);
