import { createReducers, TDataAction } from 'react-like';
import { TFileItem } from '@types';
import {
  LIST_GET_ACTION_TYPES,
  REMOVE_FILE_ACTION_TYPES,
  SET_SELECTED_ACTION_TYPE
} from '@components/list/actions';
import { DOWNLOAD_FILE_ACTION_TYPES } from '@components/downloadForm/action';

export type TListData = TFileItem[] & string;

export type TListState = {
  data: Array<TFileItem>,
  selectedItem: string,
  imgUrl: string
};

const initialState: TListState = {
  data: [],
  selectedItem: '',
  imgUrl: ''
};

const listReducers = {
  [LIST_GET_ACTION_TYPES.SUCCESS]: (_state: Partial<TListState>, action: TDataAction<TFileItem[]>) =>
    ({ data: action.data }),
  [REMOVE_FILE_ACTION_TYPES.SUCCESS]: (state: Partial<TListState>, action: TDataAction<string>) =>
    ({ data: state.data?.filter((item: TFileItem) => item.name !== action.data) }),
  [SET_SELECTED_ACTION_TYPE]: (_state: Partial<TListState>, action: TDataAction<string>) =>
    ({ selectedItem: action.data }),
  [DOWNLOAD_FILE_ACTION_TYPES.SUCCESS]: (state: Partial<TListState>, action: TDataAction<string>) =>
    ({ imgUrl: action.data || state.imgUrl })
};

const list = createReducers<TListState, TListData>(listReducers, initialState);

export default list;
