import { Store, apiMiddleware, thunkMiddleware, createConnect } from 'react-like';
import { list, TListState } from './reducers';

export type TStore = {
  list: TListState
}

const store = new Store<TStore>({ list }, [apiMiddleware, thunkMiddleware]);
const connect = createConnect(store);

export { connect };
